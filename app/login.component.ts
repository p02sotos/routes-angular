import { Component, OnInit } from '@angular/core';
import { User } from './model/User';
import { Router } from '@angular/router';
import { LoginService } from './services/login.service';

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: '../../app/templates/login.component.html',
    styleUrls: ['../../app/templates/login.component.css']
})

export class LoginComponent implements OnInit {

    user: User;
    errorMessage: string;
    submitted: false;
    loginS: false;

    constructor(
        private loginService: LoginService,
        private router: Router

    ) {

    }
    onSubmit(user: User) {
        console.log(this.user._username + this.user._password);
        this.loginService.login(this.user)
            .subscribe(data => this.router.navigate(['/admin'])
            , errMsg => {
                this.errorMessage = errMsg;
                console.log(this.errorMessage)
            });
    }
    ngOnInit(): void {
        this.user = new User();

        console.log("Iniciando Componente");
    }
}