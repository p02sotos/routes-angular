import { Component, OnInit } from '@angular/core';
import { _Route } from '../model/Route';
import { RouteService } from '../services/route.service';
import { Observable } from 'rxjs/Observable';



@Component({
  moduleId: module.id,
  selector: 'admin',
  templateUrl: '../../../app/templates/admin.component.html',
})
export class AdminComponent implements OnInit {
    routes : _Route[]; 
    errorMessage: string;
    constructor(private routeService: RouteService){}


     ngOnInit(): void {        
        console.log("Iniciando Componente de administración");
        this.routeService.getAllRoutes().subscribe(
            data => {
                this.routes = data;
                console.log(this.routes);                
            },
             error =>  this.errorMessage = <any>error);     
    }
 }
