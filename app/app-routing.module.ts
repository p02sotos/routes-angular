import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Components
import { LoginComponent }   from './login.component';
import { AdminComponent }   from './components/admin.component';
import { AppComponent }   from './app.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'login',  component: LoginComponent },
   { path: 'admin',  component: AdminComponent, canActivate: [AuthGuard] },
 
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}