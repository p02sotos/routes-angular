import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { User } from '../model/User';

import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';




@Injectable()
export class LoginService {

    public token: string;
    private loginUrl: string = "http://uroutes.sergiosoriano.com/app_dev.php/api/login_check";

    constructor(private http: Http) { };

    pingServices() { console.log("Calling login Services") }

    login(user: User): Observable<User> {
        event.preventDefault();
        console.log("En login Services");
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = JSON.stringify(user);
        console.log(body);
        return this.http.post(this.loginUrl, body, options)
            .map(this.extractData)
            .catch(this.handleError);
        ;
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('token');
    }


    private extractData(res: Response) {
        let body = res.json();
        let token = body.token;
        if(token){
            localStorage.setItem('token', token);
             this.token = token;
        }
        console.log("token: " + body.token);
        return token || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Observable.throw(errMsg);
    }
}