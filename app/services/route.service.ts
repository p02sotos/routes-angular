import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { _Route } from '../model/Route';

import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';




@Injectable()
export class RouteService {

    private routingUrl: string = "http://uroutes.sergiosoriano.com/app_dev.php/api/routes";
    private singleRouteUrl: string = "http://uroutes.sergiosoriano.com/app_dev.php/api/route";

    constructor(private http: Http) { };

    

    getAllRoutes(): Observable<_Route[]> {
        event.preventDefault();
        console.log("En login Services");
        let tokenHeader = 'Bearer '+ localStorage.getItem('token');
        let headers = new Headers({ 'X-Bearer-Token': tokenHeader });
        let options = new RequestOptions({ headers: headers });               
        return this.http.get(this.routingUrl,  options)
            .map(this.extractData)
            .catch(this.handleError);
        ;
    } 


    private extractData(res: Response) {
        let body = res.json();                    
        return body.routes || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Observable.throw(errMsg);
    }
}