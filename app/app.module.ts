import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';


//Components
import { AppComponent } from './app.component';
import { LoginComponent } from './login.component';
import { AdminComponent } from './components/admin.component';

//Services
import { LoginService } from './services/login.service';
import { RouteService } from './services/route.service';
import { AuthGuard } from './guards/auth.guard';
//Routing
import { AppRoutingModule } from './app-routing.module';



@NgModule({
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpModule,
    JsonpModule],
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,

  ],
  providers: [LoginService, AuthGuard, RouteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
